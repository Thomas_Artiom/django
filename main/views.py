from distutils.log import error
from importlib.resources import path
from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Articl
from .forms import ArticlForm
from django.views import View
from django.contrib.auth import authenticate , login
from main.forms import UserCreationForm
from django.views.generic import DetailView , UpdateView , DeleteView

class register(View):

    template_name = 'registration/register.html'

    def get(self,request):
        context = {
            'form' : UserCreationForm()
        }
        return render(request, self.template_name , context)
    
    def post(self , request):
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password1')
            return redirect('login')
        context = {
            'form' : form
        }
        return render(request, self.template_name , context)

class ToursDetailView(DetailView):
    model = Articl
    template_name = 'main/toursPodrobno.html'
    context_object_name = 'articles'

class ToursUpdateView(UpdateView):
    model = Articl
    template_name = 'main/addform.html'

    form_class = ArticlForm

class ToursDeleteView(DeleteView):
    model = Articl
    success_url = '/tours/'
    template_name = 'main/delete.html'

def index(request): 
    data = {
        'title' : 'Главная страница ',
        'values': ['some' , 'people' , 'hello'],
        'obj': {
            'date': '18.03',
            'age': '18',
            'car': 'bmw'
        }
    }
    return render(request , 'main/index.html' , data)


def tours(request):
    search_query = request.GET.get('search' , '')
    if search_query:
        tourses = Articl.objects.filter(title__icontains = search_query)
    else:
        tourses = Articl.objects.all()
    return render(request , 'main/tours.html' , {'tourses': tourses})

def toursPodrobno(request):
    tourses = Articl.objects.all()
    return render(request , 'main/toursPodrobno.html' , {'tourses': tourses})

def agenstvo(request):
    return render(request , 'main/agenstvo.html')

def toursAgenstvo(request):
    tourses = Articl.objects.all()
    return render(request , 'main/toursAgenstvo.html' , {'tourses': tourses})

def addform(request):
    error = ''
    if request.method == 'POST':
        form = ArticlForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('tours')
        else:
            error = 'Форма заполнена не верно'

    form = ArticlForm()

    data = {
        'form' : form,
        'error' : error
    }

    return render(request , 'main/addform.html' , data )