from django.db import models

class Articl(models.Model):
    title = models.CharField('Название' , max_length=50)
    price = models.CharField('Цена' , max_length=250)
    country = models.CharField('Страна' , max_length=250)
    city = models.CharField('Город' , max_length=250)
    full_text = models.TextField('Текст')
    data = models.DateTimeField('Дата публикации')

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return f'/tours/{self.id}'

    class Meta:
        verbose_name = 'Путешествие'
        verbose_name_plural = 'Путешествия '