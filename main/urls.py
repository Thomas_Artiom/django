from unicodedata import name
from xml.etree.ElementInclude import include
from django.urls import path,include
from . import views
from main.views import register

urlpatterns = [
    path('', views.index , name = "home"),
    path('tours/', views.tours , name = "tours"),
    path('tours/<int:pk>' , views.ToursDetailView.as_view() , name = 'vacase-detail'),
    path('tours/agenstvo' , views.agenstvo , name = 'agenstvo'),
    path('tours/addform' , views.addform , name = 'addform'),
    path('tours/<int:pk>/update' , views.ToursUpdateView.as_view() , name = 'tours-update'),
    path('tours/<int:pk>/delete' , views.ToursDeleteView.as_view() , name = 'tours-delete'),
    path('users/' , include('django.contrib.auth.urls')),
    path('register/' , register.as_view() , name = 'register'),
    path('tours/agenstvo/toursAgenstvo' , views.toursAgenstvo , name = 'toursAgenstvo')
]